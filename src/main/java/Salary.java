import java.util.*;

public class Salary {

    public static int getTotalSalary(int[] week){

        int no_of_days=7;
        int total_hours_worked=0, total_salary, extra_hours_worked=0;

        for(int i=0; i<no_of_days; i++){
            total_hours_worked+= week[i];
            if(week[i]>8)
                extra_hours_worked+= (week[i]-8);
        }

        total_salary = total_hours_worked*100 + extra_hours_worked*15;

        total_hours_worked -= (week[0]+week[no_of_days-1]);

        if(total_hours_worked>40)
            total_salary += (total_hours_worked-40)*25;

        if(week[0]!=0)
            total_salary += (week[0]*0.5*100);

        if(week[no_of_days-1]!=0)
            total_salary += (week[no_of_days-1]*0.25*100);

        return total_salary;

    }

    public static void main(String[] args) {

         Scanner sc= new Scanner(System.in);

         int no_of_days=7;
         int[] week= new int[no_of_days];
         for(int i=0; i<no_of_days; i++)
             week[i]= sc.nextInt();

         int total_salary= getTotalSalary(week);

         System.out.println(total_salary);

    }
}
